<header>
    <h1><?= $title ?></h1>
    <div class="links-bar">
        <?= $headerLink1 ?>
        <?= $headerLink2 ?>
        <?= $headerLink3 ?>
    </div>
</header>
