<?php
$title='Homework page title';
$headerLink1="<a href='#'>Link 1</a>";
$headerLink2="<a href='#'>Link 2</a>";
$headerLink3="<a href='#'>Link 3</a>";

$menuTitle="<h1>Page menu</h1>";
$banner1="<div class='banner'><a href='#'>banner text 1</a></div>";
$banner2="<div class='banner'><a href='#'>banner text 2</a></div>";
$banner3="<div class='banner'><a href='#'>banner text 3</a></div>";

$footerInfo="<p>Page homework made 27.03.2021</p>";
$footerLink1="<a href='#'>Link 1</a>";
$footerLink2="<a href='#'>Link 2</a>";
$footerLink3="<a href='#'>Link 3</a>";

$post1="<div class='post'><h1>Test post 1</h1><p>Test text 1</p></div>";
$post2="<div class='post'><h1>Test post 2</h1><p>Test text 2</p></div>";
$post3="<div class='post'><h1>Test post 3</h1><p>Test text 3</p></div>";
$post4="<div class='post'><h1>Test post 4</h1><p>Test text 4</p></div>";
?>

<html>
<head>
    <title>Homework 1</title>
    <style>
        *{
            margin:0;
            padding:0;
            box-sizing: border-box;
        }
        header{
            background-color: #e2a300;
            border-bottom: 4pt solid #F8F8F8;
            padding: 15px;
            width: 100vw;
            text-align: center;
        }
        header h1{
            font-family: "Segoe UI";
            color: #F8F8F8;
            font-weight: normal;
        }
        header .links-bar{
            display: block;
            margin: 0 5px;
            padding-top: 5px;
            border-top: 1px solid #FFFFFF;
        }
        main{
            display: flex;
            flex-direction: row;
        }
        .menu{
            background-color: #e2a300;
            padding: 5px;
            width: 20vw;
            height: auto;
        }
        .menu h1{
            font-family: "Segoe UI";
            color: #F8F8F8;
            font-weight: normal;
            text-align: center;
            font-size: 14pt;
        }
        .menu li{
            background-color: #ffb500;
            font-family: "Segoe UI";
            margin: 5px;
            padding: 3px;
            text-align: center;
        }
        a{
            text-decoration: none;
        }
        .banner{
            margin: 3px auto;
            text-align: center;
            font-size: 10pt;
            text-transform: uppercase;
            font-family: Tahoma;
            background-color: #FFFFFF;
            color: #000000;
            border: 1px solid #000000;
            width: 120px;
            padding: 10px;
        }
        .content{
            background-color: #d1d1d1;
            padding: 5px;
            width: 80vw;
            height: auto;
        }
        .post{
            margin: 10px;
            padding: 10px;
            background-color: #FFFFFF;
        }
        footer{
            border-top: 4pt solid #F8F8F8;
            width: 100vw;
            background-color: #e2a300;
            font-family: "Segoe UI";
            text-align: center;
            padding: 10px 0;
        }
        footer .links-bar{
    margin: 0 5px;
            padding-bottom: 5px;
            border-bottom: 1px solid #FFFFFF;
        }
    </style>
</head>
</head>
<body>
<?php include __DIR__. '/header.php'; ?>
<main>
    <?php include __DIR__. '/sidebar.php'; ?>
    <?php include __DIR__. '/content.php'; ?>
</main>
<?php include __DIR__. '/footer.php'; ?>
</body>
</html>